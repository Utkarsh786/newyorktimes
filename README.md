Project Name : NewYork Times

Project Description  :  NYTimes is an iOS application which shows the most popular topics from NewYork Times of last 7 days.For using the data from NYTimes public api we need a apikey from their developer website.Below is the link for getting the apikey:

Link : https://developer.nytimes.com/signup

Requirement : 

iOS10.0  or greater devcie 
Xcode 10.0 or greater 

Run on iOS Simulator/iOS device

Support only for iPhone

Alamofire : Alamofire is used for make a connection between the Client and Server.It is used for getting the response from server.Behind this librarey URLSession isused

SBWebImage : SBWebImage is used for showing the image url into the UIImageView

MBProgressHUD : MBProgressHUD is used for showing the loading indicator in the application 

SwiftyJson : SwiftyJson is used for parsing the JSON objects and showing into the UI







