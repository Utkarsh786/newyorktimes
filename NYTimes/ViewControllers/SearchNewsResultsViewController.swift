//
//  SearchNewsResultsViewController.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import MBProgressHUD

class SearchNewsResultsViewController: UIViewController {
    
    var strSearchField : String? = nil
    var arrNewsFeedInfo = [SearchNewsFeed]()
    @IBOutlet weak var tblView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Regsiter Nib //
        let nib = UINib(nibName: "NewsFeedDetailsTableViewCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "NewsFeedDetailsTableViewCell")
        self.tblView.estimatedRowHeight = 70.0;
        tblView.tableFooterView = UIView()

        self.getNYTimesMostPopularNewsFeed()
        self.tblView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.title = strSearchField!
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getNYTimesMostPopularNewsFeed()  {
        MBProgressHUD .showAdded(to: self.view, animated: true);
        
        if Reachability.isConnectedToNetwork() == true{
            let  apiParam  = "articlesearch.json?q=\(strSearchField!)&api-key=\(Key.NYTimes.ApiKey)"
            AFWrapper.getSearchNewsFeed(page: apiParam,success: {
                (JSONResponse) -> Void in
                print(JSONResponse)
                
                DispatchQueue.main.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
                let sStatus = JSONResponse["status"].stringValue
                if sStatus == "OK"{
                    if JSONResponse["response"]["docs"].arrayValue.count > 0 {
                        //Parsed Successfull
                        for newsFeedInfo in JSONResponse["response"]["docs"].arrayValue{
                            let newsFeedInfo = SearchNewsFeed.init(json: newsFeedInfo)
                            self.arrNewsFeedInfo.append(newsFeedInfo)
                        }
                        self.tblView.reloadData()
                    }
                    else{
                        DispatchQueue.main.async {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                                HelperClass().showAlertDialog(strMessage: NSLocalizedString("Result_Found", comment: ""))
                            })
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                            HelperClass().showAlertDialog(strMessage: NSLocalizedString("Response_Error", comment: ""))
                            
                        })
                    }
                }
            }) {
                (error) -> Void in
                print(error)
                DispatchQueue.main.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        HelperClass().showAlertDialog(strMessage: NSLocalizedString("Response_Error", comment: ""))
                    })
                }
            }
        }
        else{
            // Network Connectivity Not Found
            
            DispatchQueue.main.async {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    HelperClass().showAlertDialog(strMessage: NSLocalizedString("Network_Error", comment: ""))
                    
                })
            }
        }
    }
}



extension SearchNewsResultsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrNewsFeedInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleTableViewCell", for: indexPath) as! SimpleTableViewCell
        let searchNewsData = self.arrNewsFeedInfo[indexPath.row]
        cell.m_lbl?.text = searchNewsData.title
        return cell
    }
}

extension SearchNewsResultsViewController : UITableViewDelegate{
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
        //  return UITableView.automaticDimension
    }
}

