//
//  NewsArticlesViewController.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit

class NewsArticlesViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
        
        self.navigationItem.title = NSLocalizedString("Title", comment: "")
        
        tblView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK:- UITableView DataSource and Delegate Methods

extension NewsArticlesViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return 1
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        cell.backgroundColor = UIColor.white
        cell.accessoryType = .disclosureIndicator
        
        if indexPath.section == 0 {
            cell.textLabel?.text = NSLocalizedString("NYTimes_Search", comment: "")
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                cell.textLabel?.text = NSLocalizedString("Most_Viewed", comment: "")
            }else if indexPath.row == 1{
                cell.textLabel?.text = NSLocalizedString("Most_Shared", comment: "")
            } else if indexPath.row == 2{
                cell.textLabel?.text = NSLocalizedString("Most_Emailed", comment: "")
            }
        }
        
        return cell
    }
}


extension NewsArticlesViewController : UITableViewDelegate{
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(indexPath.section == 0){
            let viewController =  self.storyboard?.instantiateViewController(withIdentifier: "SearchNewsViewController") as! SearchNewsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if indexPath.section == 1  {
            let viewController =  self.storyboard?.instantiateViewController(withIdentifier: "NewsFeedViewController") as! NewsFeedViewController
            switch indexPath.row {
            case 0 :
                viewController.newsType = "mostviewed"
            case 1 :
                viewController.newsType = "shared"
            case 2 :
                viewController.newsType = "emailed"
            default:
                viewController.newsType = ""
            }
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        if section == 0 {
            return NSLocalizedString("NYTimes_Search", comment: "")
        }
        return NSLocalizedString("NYTimes_Popular", comment: "")
    }
}




