//
//  SearchNewsViewController.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit

class SearchNewsViewController: UIViewController {
    
    @IBOutlet weak var txtField : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("NYTimes_Search_News", comment: "")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: // UIControlMethods
    
    @IBAction func onSearchButtonPressed(){
        
        if txtField.text == "" {
            HelperClass().showAlertDialog(strMessage: NSLocalizedString("TextFiled_Error", comment: ""))
        }else {
            
            let viewController =  self.storyboard?.instantiateViewController(withIdentifier: "SearchNewsResultsViewController") as! SearchNewsResultsViewController
            viewController.strSearchField = txtField.text
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
