//
//  NewsFeedDetailsViewController.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit

class NewsFeedDetailsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var m_CollectionView: UICollectionView!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_tblView: UITableView!
    
    var arrKeywords = [String]()
    
    var newsFeedData : NewsFeedInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.title = newsFeedData.title
        arrKeywords = newsFeedData.adxKeyword.components(separatedBy: ";")
        m_tblView.tableFooterView = UIView()

        if(self.newsFeedData.arrImages.count > 0) {
            let imgUrl = self.newsFeedData.arrImages[0]
            m_imgView?.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "defaultimgplaceholder"))
        }
        else{
            // No Image Found
            m_imgView.image = UIImage(named: "defaultimgplaceholder")// Show Default Image
        }
        
        // Regsiter Nib //
        let nib = UINib(nibName: "NewsFeedDetailsTableViewCell", bundle: nil)
        self.m_tblView.register(nib, forCellReuseIdentifier: "NewsFeedDetailsTableViewCell")
        
        self.m_tblView.estimatedRowHeight = 70.0;        
        self.m_tblView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK:- UITableView DataSource and Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 1 {
            return  arrKeywords.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleTableViewCell", for: indexPath) as! SimpleTableViewCell
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedDetailsTableViewCell", for: indexPath) as! NewsFeedDetailsTableViewCell
            cell.setNewsFeedData(dataNewsFeed: newsFeedData)
            
            return cell
            
        }else if indexPath.section == 1 {
            cell.m_lbl.text = arrKeywords[indexPath.row]
            cell.m_lbl.textAlignment = .center
            
        }
        else if indexPath.section == 2 {
            cell.m_lbl.textAlignment = .center
            cell.m_lbl.text = NSLocalizedString("Safari", comment: "")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 2 {
            UIApplication.shared.openURL(NSURL(string: newsFeedData.websiteURL)! as URL)
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
       return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        if section == 0 {
            return "Published Date : \(newsFeedData.date)"
            
        } else if section == 1 {
            return NSLocalizedString("Keywords", comment: "")
            
        } else if section == 2 {
            return NSLocalizedString("Website", comment: "")
            
        }
        return newsFeedData.date
    }
}
