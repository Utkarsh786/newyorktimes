//
//  ViewController.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewsFeedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    var arrNewsFeedInfo = [NewsFeedInfo]()
    var newsType : String? = nil
    var newsTitle : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        arrNewsFeedInfo = Array()
        let nib = UINib(nibName: "NewsFeedTableViewCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "NewsFeedTableViewCell")
        
        self.getNYTimesMostPopularNewsFeed(strTag: newsType!, strTime: "7")
        let str  = getNavigationTitle(strTitle: newsType!)
        self.navigationItem.title = str
        self.navigationItem.hidesBackButton = false
        
        tblView.tableFooterView = UIView()

    }
    
    //MARK: - Class Methods
    
    func getNavigationTitle(strTitle: String) -> String {
        var str : String? = nil
        if(strTitle == "mostviewed"){
            str = "Most Viewed"
        }else {
            str = "Most \(strTitle)"
        }
        return str!
    }
    
    func getNYTimesMostPopularNewsFeed(strTag:String, strTime:String)  {
        MBProgressHUD .showAdded(to: self.view, animated: true);
        
        if Reachability.isConnectedToNetwork() == true{
            var apiParam = ""
            if(strTag == "mostviewed"){
                apiParam = "\(strTag)/all-sections/\(strTime).json?api-key=\(Key.NYTimes.ApiKey)"
            }else if(strTag == "emailed"){
                apiParam = "\(strTag)/7.json?api-key=\(Key.NYTimes.ApiKey)"
            }else if(strTag == "shared"){
                apiParam = "\(strTag)/1/facebook.json?api-key=\(Key.NYTimes.ApiKey)"
            }
            AFWrapper.getNYTimesMostPopularNewsFeed(page: apiParam,success: {
                (JSONResponse) -> Void in
                print(JSONResponse)
                
                DispatchQueue.main.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
                
                let sStatus = JSONResponse["status"].stringValue
                
                if sStatus == "OK"{
                    if JSONResponse["results"].arrayValue.count > 0 {
                        //Parsed Successfull
                        for newsFeedInfo in JSONResponse["results"].arrayValue{
                            let newsFeedInfo = NewsFeedInfo.init(json: newsFeedInfo)
                            self.arrNewsFeedInfo.append(newsFeedInfo)
                        }
                        self.tblView.reloadData()
                    }
                    else{
                        DispatchQueue.main.async {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                               HelperClass().showAlertDialog(strMessage: NSLocalizedString("Result_Found", comment: ""))
                            })
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                            HelperClass().showAlertDialog(strMessage: NSLocalizedString("Response_Error", comment: ""))
                            
                        })
                    }
                }
            }) {
                (error) -> Void in
                print(error)
                DispatchQueue.main.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        HelperClass().showAlertDialog(strMessage: NSLocalizedString("Response_Error", comment: ""))
                    })
                }
            }
        }
        else{
            // Network Connectivity Not Found
            
            DispatchQueue.main.async {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    HelperClass().showAlertDialog(strMessage: NSLocalizedString("Network_Error", comment: ""))
                    
                })
            }
        }
    }
    
    //MARK: - UITableView DataSource and Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNewsFeedInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewCell", for: indexPath) as! NewsFeedTableViewCell
        let data = self.arrNewsFeedInfo[indexPath.row]
        cell.setNewsFeedData(dataNewsFeed: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        let newsDetailsViewController =  self.storyboard?.instantiateViewController(withIdentifier: "NewsFeedDetailsViewController") as! NewsFeedDetailsViewController
        let newsFeedData = self.arrNewsFeedInfo[indexPath.row]
        newsDetailsViewController.newsFeedData = newsFeedData
        self.navigationController?.pushViewController(newsDetailsViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 105.0;
    }
}
