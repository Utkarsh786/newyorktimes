//
//  NewsFeedInfo.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class NewsFeedInfo: NSObject {
    var title :String =  ""
    var Description : String = ""
    var date : String = ""
    var arrImages = [String]()
    var adxKeyword : String = ""
    var websiteURL : String = ""
    
    required init(json:JSON){
        self.title = json["title"].stringValue
        self.Description = json["abstract"].stringValue
        self.date = json["published_date"].stringValue
        self.adxKeyword = json["adx_keywords"].stringValue
        self.websiteURL = json["url"].stringValue
        
        if json["media"].arrayValue.count > 0 {
            for arrImg in json["media"].arrayValue{
                for data in arrImg["media-metadata"].arrayValue{
                    let str = data["url"].stringValue
                    self.arrImages.append(str)
                }
            }
        }
    }
}
