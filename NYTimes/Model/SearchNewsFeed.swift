//
//  NewsFeedInfo.swift
//  NYTimes
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class SearchNewsFeed: NSObject {
    var title :String =  ""
    
    required init(json:JSON){
        self.title = json["abstract"].stringValue
    }
}
