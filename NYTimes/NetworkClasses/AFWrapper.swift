//
//  AFWrapper.swift
//  AQISwift
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject {
    
    class func getNYTimesMostPopularNewsFeed(page : String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let str = NSString.init(format: "http://api.nytimes.com/svc/mostpopular/v2/%@", page)
        
        Alamofire.request(str as String, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func getSearchNewsFeed(page : String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let str = NSString.init(format: "https://api.nytimes.com/svc/search/v2/%@", page)
        
        Alamofire.request(str as String, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}
