//
//  CommentTableViewCell.swift
//  NearBy
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit

class NewsFeedDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet var m_lblTitle : UILabel!
    @IBOutlet var m_lblDescription : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setNewsFeedData(dataNewsFeed:NewsFeedInfo)  {
        m_lblTitle.text = dataNewsFeed.title
        m_lblDescription.text = dataNewsFeed.Description
    }
}
