//
//  CommentTableViewCell.swift
//  NearBy
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NewsFeedTableViewCell: UITableViewCell {
    
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDescription : UILabel!
    @IBOutlet var lblDate : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.clipsToBounds = true;
        imgView.layer.cornerRadius = 35;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setNewsFeedData(dataNewsFeed:NewsFeedInfo)  {
        lblTitle.text = dataNewsFeed.title
        lblDescription.text = dataNewsFeed.Description
        lblDate.text = dataNewsFeed.date
        if(dataNewsFeed.arrImages.count > 0){
            imgView?.sd_setImage(with: URL(string: dataNewsFeed.arrImages[0]), placeholderImage: UIImage(named: "NewsFeed"))
        }else{
            imgView.image = UIImage(named: "NewsFeed")
        }
    }
}
