//
//  NYTimesTests.swift
//  NYTimesTests
//
//  Created by Utkarsh on 02/07/19.
//  Copyright © 2019 Utkarsh. All rights reserved.
//

import XCTest

@testable import NYTimes


class NYTimesTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testGitUserData() {
        guard let gitUrl = URL(string: "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/30.json?api-key=FAQ3vltcAWiy9W3yagFHMeAnTrdXy7pr") else { return }
        let promise = expectation(description: "Normal Request")
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                if let result = json as? NSDictionary {
                    XCTAssertTrue(result["copyright"] as! String == "Copyright (c) 2018 The New York Times Company.  All Rights Reserved.")
                    XCTAssertTrue(result["status"] as! String == "OK")
                    promise.fulfill()
                }
            } catch let err {
                print("Err", err)
            }
            }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
}
